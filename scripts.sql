INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(1, 2, '09:00:00', '10:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(2, 3, '10:00:00', '11:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(3, 4, '12:00:00', '13:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(4, 5, '13:00:00', '14:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(5, 6, '14:00:00', '15:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(6, 7, '15:00:00', '16:00:00', 'Saturday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(7, 2, '09:00:00', '10:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(8, 3, '10:00:00', '11:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(9, 4, '12:00:00', '13:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(10, 5, '13:00:00', '14:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(11, 6, '14:00:00', '15:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(12, 7, '15:00:00', '16:00:00', 'Monday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(13, 2, '09:00:00', '10:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(14, 3, '10:00:00', '11:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(15, 4, '12:00:00', '13:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(16, 5, '13:00:00', '14:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(17, 6, '14:00:00', '15:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(18, 7, '15:00:00', '16:00:00', 'Tuesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(19, 2, '09:00:00', '10:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(20, 3, '10:00:00', '11:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(21, 4, '12:00:00', '13:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(22, 5, '13:00:00', '14:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(23, 6, '14:00:00', '15:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(24, 7, '15:00:00', '16:00:00', 'Wednesday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(25, 2, '09:00:00', '10:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(26, 3, '10:00:00', '11:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(27, 4, '12:00:00', '13:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(28, 5, '13:00:00', '14:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(29, 6, '14:00:00', '15:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(30, 7, '15:00:00', '16:00:00', 'Thursday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(31, 2, '09:00:00', '10:00:00', 'Friday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(32, 3, '10:00:00', '11:00:00', 'Friday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(33, 4, '12:00:00', '13:00:00', 'Friday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(34, 5, '13:00:00', '14:00:00', 'Friday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(35, 6, '14:00:00', '15:00:00', 'Friday', 'Active', 1);
INSERT INTO time_tables (id, subject_id, start_time, end_time, week_day, status, campus_id) VALUES(36, 7, '15:00:00', '16:00:00', 'Friday', 'Active', 1);

<?php ?>
<section class="content-header">
<h1>Total Dues</h1>
</section>
<!-- Main content -->
<section class="content"> <!-- /.row -->
<div class="row">
	<div class="col-lg-12">
	<?php $this->load->view('students/readonly/fee_dues'); ?>

	</div>
</div>
<div class="row">
	<div class="col-lg-12">
	<?php $this->load->view('students/readonly/stationary_dues'); ?>

	</div>
</div>
</section>

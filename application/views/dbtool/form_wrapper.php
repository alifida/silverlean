<?php ?>
<?php $this->load->view('dbtool/top_section'); ?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-danger">
				<!-- Default panel contents -->
				<div class="box-header"></div>
				<div class="box-body">
					<?php $this->load->view('dbtool/form'); ?>
				</div>
			</div>
		</div>
	</div>
</section>


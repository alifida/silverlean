<?php ?>

<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Invoices</h1>
</section>

<!-- Main content -->
<section class="content"> <!-- Main row -->
<div class="row">
	<div class="col-lg-12">
	<?php $this->load->view('invoices/list'); ?>
	</div>
</div>

</section>
<!-- /.content -->

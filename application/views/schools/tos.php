<?php
?>
<p>
						
						
						<h1>e-karobar.com Terms of Service</h1>
						<b>Please read this agreement ("TOS") carefully. By using or
							accessing any Service, you (as the "User") agree to the terms of
							this TOS. "Service" means the e-karobar.com service. If you do
							not agree, do not use this Service.</b> <br /> This TOS is a
						legal agreement between you and E-karobar Inc. This TOS governs
						the Service, and content available through it. It also governs
						support services (if any) available as part of your subscription.
						If a separate or supplemental TOS appears when you access any
						element of the Service, its terms will control as to that element.
						E-karobar Inc. reserves the right to update and change the Terms
						of Service from time to time without notice. Any new features that
						augment or enhance the current Service, including the release of
						new tools and resources, shall be subject to the Terms of Service.
						Continued use of the Service after any such changes shall
						constitute your consent to such changes. You can review the most
						current version of the Terms of Service at any time at:
						http://e-karobar.com/terms Violation of any of the terms below
						will result in the termination of your Account. While E-karobar
						Inc. prohibits such conduct and Content on the Service, you
						understand and agree that E-karobar Inc. cannot be responsible
						for the Content posted on the Service and you nonetheless may be
						exposed to such materials. You agree to use the Service at your
						own risk.
						<h1>Account Terms</h1>
						<ol>
							<li>You must be a human. Accounts registered by "bots" or other
								automated methods are not permitted.</li>
							<li>You must provide your legal school name, a valid email
								address, and any other information requested in order to
								complete the signup process.</li>
							<li>Your login may only be used by one person – a single login
								shared by multiple people is not permitted. You may create
								separate logins for as many people as you'd like.</li>
							<li>You are responsible for maintaining the security of your
								account and password. E-karobar Inc cannot and will not be
								liable for any loss or damage from your failure to comply with
								this security obligation.</li>
							<li>You are responsible for all Content posted and activity that
								occurs under your account (even when Content is posted by others
								who have accounts under your account).</li>
							<li>One person or legal entity may not maintain more than one
								free account.</li>
							<li>You may not use the Service for any illegal or unauthorized
								purpose. You must not, in the use of the Service, violate any
								laws in your jurisdiction (including but not limited to
								copyright laws).</li>
						</ol>
						<h1>Payment, Refunds, Upgrading and Downgrading Terms</h1>
						<ol>
							<li>You must represent a school or a group of schools ("School")
								in order to qualify for the 1-month free trial period.</li>
							<li>Your School can fully utilize the 1-month free trial period
								to examine all features of the Service.</li>
							<li>If you need more time beyond the 1-month free trial period,
								contact E-karobar Inc. and tell us why. We reserve the right to
								refuse or approve the extension request.</li>
							<li>After the free trial period has ended, your School may choose
								to terminate the account or continue on a paying account.</li>
							<li>Once your account is converted to a paying account, your free
								trial period is over. You will be billed for your first month
								immediately upon conversion.</li>
							<li>The Service is billed in advance on a monthly basis. There
								will be no refunds or credits for partial months of service,
								upgrade/downgrade refunds, or refunds for months unused with an
								open account. For other reasons not mentioned above, then
								refunds will be given at the discretion of the Company
								Management.
							
							<li>Monthly bills are payable in full 10 days after the bill date
								where checks are used to make payments.</li>
							<li>If we do not receive payment by the due date, the system will
								automatically lock-out your account. Users under the account
								will not be able to log-in until the outstanding payment is
								received in full. If the full outstanding payment is not
								received within 30 days after the due date, then your account
								and all data contained within will be deleted.</li>
							<li>You can cancel at any time. No further payments are required
								from you upon account cancellation.</li>
							<li>All fees are exclusive of all taxes, levies, or duties
								imposed by taxing authorities, and you shall be responsible for
								payment of all such taxes, levies, or duties.</li>
							<li>For any upgrade or downgrade in plan level, you will
								automatically be billed the new rate on your next billing cycle.</li>
							<li>Downgrading your Service may cause the loss of Content,
								features, or capacity of your Account. E-karobar Inc. does not
								accept any liability for such loss.</li>
						</ol>
						<h1>Cancellation and Termination</h1>

						<ol>
							<li>You are solely responsible for properly cancelling your
								account, which can be done via email to cancel at
								e-karobar.com.</li>
							<li>All of your Content will be immediately deleted from the
								Service upon cancellation. This information cannot be recovered
								once your account is cancelled.</li>
							<li>If you cancel the Service before the end of your current paid
								up month, your cancellation will take effect immediately and you
								will not be charged again.</li>
							<li>E-karobar Inc., in its sole discretion, has the right to
								suspend or terminate your account and refuse any and all current
								or future use of the Service, or any other E-karobar Inc.
								service, for any reason at any time. Such termination of the
								Service will result in the deactivation or deletion of your
								Account or your access to your Account, and the forfeiture and
								relinquishment of all Content in your Account. E-karobar Inc.
								reserves the right to refuse service to anyone for any reason at
								any time.</li>
						</ol>

						<h1>Modifications to the Service and Prices</h1>
						<ol>
							<li>E-karobar Inc. reserves the right at any time and from time
								to time to modify or discontinue, temporarily or permanently,
								the Service (or any part thereof) with or without notice.</li>
							<li>Prices of all Services, including but not limited to monthly
								subscription plan fees to the Service, are subject to change
								upon 30 days notice from us. Such notice may be provided at any
								time by posting the changes to the e-karobar.com site.</li>
							<li>E-karobar Inc. shall not be liable to you or to any third
								party for any modification, price change, suspension or
								discontinuance of the Service. Copyright and Content Ownership</li>
							<li>We claim no intellectual property rights over the material
								you provide to the Service. Your profile and materials uploaded
								remain yours.</li>
							<li>E-karobar Inc. does not pre-screen Content, but E-karobar
								Inc. and its designee have the right (but not the obligation) in
								their sole discretion to refuse or remove any Content that is
								available via the Service.</li>
							<li>The look and feel of the Service is copyright©2015
								E-karobar Inc. All rights reserved. You may not duplicate,
								copy, or reuse any portion of the HTML/CSS, Flash pages or
								visual design elements without express written permission from
								E-karobar Inc.</li>
						</ol>
						<h1>General Conditions</h1>
						<ol>
							<li>Your use of the Service is at your sole risk. The service is
								provided on an "as is" and "as available" basis.
							
							<li>Technical support is only provided to paying account holders
								and is only available via email.</li>
							<li>You understand that E-karobar Inc. uses third party vendors
								and hosting partners to provide the necessary hardware,
								software, networking, storage, and related technology required
								to run the Service.</li>
							<li>You must not modify, adapt or hack the Service or modify
								another website so as to falsely imply that it is associated
								with the Service, E-karobar Inc., or any other E-karobar Inc.
								service.</li>
							<li>You agree not to reproduce, duplicate, copy, sell, resell or
								exploit any portion of the Service, use of the Service, or
								access to the Service without the express written permission by
								E-karobar Inc.</li>
							<li>We may, but have no obligation to, remove Content and
								Accounts containing Content that we determine in our sole
								discretion are unlawful, offensive, threatening, libelous,
								defamatory, pornographic, obscene or otherwise objectionable or
								violates any party’s intellectual property or these Terms of
								Service.</li>
							<li>Verbal, physical, written or other abuse (including threats
								of abuse or retribution) of any E-karobar Inc. customer,
								employee, member, or officer will result in immediate account
								termination.</li>
							<li>You understand that the technical processing and transmission
								of the Service, including your Content, may be transferred
								unencrypted and involve (a) transmissions over various networks;
								and (b) changes to conform and adapt to technical requirements
								of connecting networks or devices.</li>
							<li>You must not upload, post, host, or transmit unsolicited
								email, SMSs, or "spam" messages.</li>
							<li>You must not transmit any worms or viruses or any code of a
								destructive nature.</li>
							<li>If your bandwidth usage exceeds 300 MB/month, or
								significantly exceeds the average bandwidth usage (as determined
								solely by E-karobar Inc.) of other e-karobar.com customers, we
								reserve the right to immediately disable your account or
								throttle your file hosting until you can reduce your bandwidth
								consumption.</li>
							<li>E-karobar Inc. does not warrant that (i) the service will
								meet your specific requirements, (ii) the service will be
								uninterrupted, timely, secure, or error-free, (iii) the results
								that may be obtained from the use of the service will be
								accurate or reliable, (iv) the quality of any products,
								services, information, or other material purchased or obtained
								by you through the service will meet your expectations, and (v)
								any errors in the Service will be corrected.</li>
							<li>You expressly understand and agree that E-karobar Inc. shall
								not be liable for any direct, indirect, incidental, special,
								consequential or exemplary damages, including but not limited
								to, damages for loss of profits, goodwill, use, data or other
								intangible losses (even if E-karobar Inc. has been advised of
								the possibility of such damages), resulting from: (i) the use or
								the inability to use the service; (ii) the cost of procurement
								of substitute goods and services resulting from any goods, data,
								information or services purchased or obtained or messages
								received or transactions entered into through or from the
								service; (iii) unauthorized access to or alteration of your
								transmissions or data; (iv) statements or conduct of any third
								party on the service; (v) or any other matter relating to the
								service.</li>
							<li>The failure of E-karobar Inc. to exercise or enforce any
								right or provision of the Terms of Service shall not constitute
								a waiver of such right or provision. The Terms of Service
								constitutes the entire agreement between you and E-karobar Inc.
								and govern your use of the Service, superseding any prior
								agreements between you and E-karobar Inc. (including, but not
								limited to, any prior versions of the Terms of Service).</li>
							<li>Questions about the Terms of Service should be sent to:
								support@e-karobar.com.</li>
						</ol>



						</p>
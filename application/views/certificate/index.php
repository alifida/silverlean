<?php ?>

<section class="content-header">
	<h1>Certificates</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12" id="certificate_area">
			<?php $this->load->view('certificate/list'); ?>
		</div>
	</div>
</section>


<?php ?>

<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 <a target="_blank" href="http://e-karobar.com/" title="E-karobar">E-karobar</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <img class="pull-right" src="<?= base_url() ?>public/images/e-karobar.png" alt="E-karobar" title="E-karobar">
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
 
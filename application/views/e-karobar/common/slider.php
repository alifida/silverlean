<?php ?>
<section id="main-slider" class="carousel">
        <div class="carousel-inner">

            <div class="item active">
                <div class="container">
                    <div class="carousel-content">
                        <div class="row">
                        	<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 ">
    	                    	<div class="monitor_frame">
	    	                    	<img src="<?= base_url() ?>public/images/banner/banner_4.png" alt="" >
    	                    	</div>
	                        </div>
    	                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 ">
                        		<h1>Online School Management System</h1>
		                        <p class="lead">Most Secure, Reliable, User friendly.</p>
	                        </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item">
                <div class="container">
                    <div class="row">
    	                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 ">
                        		<h1>Online School Management System</h1>
		                        <p class="lead">Most Secure, Reliable, User friendly.</p>
	                        </div>
    	                    <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 ">
    	                    	<div class="monitor_frame">
	    	                    	<img src="<?= base_url() ?>public/images/banner/banner_1.png" alt="" >
    	                    	</div>
	                        </div>
                        </div>
                </div>
            </div><!--/.item-->
            <div class="item">
                <div class="container">
                    <div class="carousel-content">
                        <div class="row">
    	                    
    	                    <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 ">
    	                    	<div class="monitor_frame">
	    	                    	<img src="<?= base_url() ?>public/images/banner/banner_3.png" alt="" >
    	                    	</div>
	                        </div>
	                        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 ">
                        		<h1>Online School Management System</h1>
		                        <p class="lead">Most Secure, Reliable, User friendly.</p>
	                        </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->

        </div><!--/.carousel-inner-->
        <a class="prev" href="#main-slider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" ></span></a>
        <a class="next" href="#main-slider" data-slide="next"><span class="glyphicon glyphicon-chevron-right" ></span></a>
    </section><!--/#main-slider-->
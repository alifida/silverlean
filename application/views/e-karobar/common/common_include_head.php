<?php ?>

	<link href="<?= base_url() ?>public/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/css/font-awesome.min1.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?= base_url() ?>public/css/main.css" rel="stylesheet">
	<title><?= get_app_message("app.page.title")?></title>
<?php ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
.play-btn {
	position: absolute;
	top: 50%;
	display: inline-block;
	left: 50%;
	margin-top: -25px;
	margin-left: -35px;
}

.play-btn i {
	font-size: 50px;
}

.blur-image {
	position: absolute;
	top: 0;
	left: 0;
	background-color: #fff;
	height: 100%;
	width: 100%;
	opacity: 0.4;
}
.vertical-margin {
	margin: 5px 0px 30px 0px;
}

.video-thumb-wrapper{
	border: 2px #EF4F4A solid;
}

</style>
<?php $this->load->view('e-karobar/common/common_include_head'); ?>
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<!--/head-->

<body data-spy="scroll" data-target="#navbar" data-offset="0">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<?php $this->load->view('e-karobar/common/top_menu'); ?>
	<br />


	<section id="modules">
		<div class="container">
			<div class="box">
				<div class="center gap">
					<h2>Video Demonstration</h2>
					<p class="lead">Following are a few of the video guide for the users.</p>
				</div>
				<!--/.center-->

				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row ">
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 ">
								<div class="video-thumb-wrapper">
									<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-video" data-allowfullscreen="true" data-href="/815488331878130/videos/vb.815488331878130/820724391354524/?type=1"><div class="fb-xfbml-parse-ignore"><blockquote cite="/815488331878130/videos/820724391354524/"><a href="/815488331878130/videos/820724391354524/"></a><p>New School Registration, Classes and Fee Management, Student Management.</p>Posted by <a href="https://www.facebook.com/pages/School-Management-System/815488331878130">E-karobar</a> on Monday, April 27, 2015</blockquote></div></div>
								</div>
							</div>
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 vertical-margin center">
								<a href="javascript:void(0)" class="btn btn-primary">Sign-up and Basic Configuration</a>
							</div>
						</div>
					</div>
					
					
					
					<div class="col-lg-12 col-md-12">
						<div class="row ">
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 ">
								<div class="video-thumb-wrapper">
									<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-video" data-allowfullscreen="true" data-href="https://www.facebook.com/815488331878130/videos/821116184648678/"><div class="fb-xfbml-parse-ignore"><blockquote cite="/815488331878130/videos/821116184648678/"><a href="/815488331878130/videos/821116184648678/"></a><p>Employee Management</p>Posted by <a href="https://www.facebook.com/pages/School-Management-System/815488331878130">E-karobar</a> on Tuesday, April 28, 2015</blockquote></div></div>
								</div>
							</div>
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 vertical-margin center">
								<a href="javascript:void(0)" class="btn btn-primary">Employee Management</a>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1  ">
								<div class="video-thumb-wrapper">
									 <div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-video" data-allowfullscreen="true" data-href="https://www.facebook.com/815488331878130/videos/821633924596904/"><div class="fb-xfbml-parse-ignore"><blockquote cite="/815488331878130/videos/821633924596904/"><a href="/815488331878130/videos/821633924596904/"></a><p>Attendance Register</p>Posted by <a href="https://www.facebook.com/pages/School-Management-System/815488331878130">School Management System</a> on Wednesday, April 29, 2015</blockquote></div></div>
								</div>
							</div>
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 vertical-margin center">
								<a href="javascript:void(0)" class="btn btn-primary">Attendance Register </a>
							</div>
							
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1  ">
								<div class="video-thumb-wrapper">
									 <div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-video" data-allowfullscreen="true" data-href="/815488331878130/videos/vb.815488331878130/825337397559890/?type=1"><div class="fb-xfbml-parse-ignore"><blockquote cite="/815488331878130/videos/825337397559890/"><a href="/815488331878130/videos/825337397559890/"></a><p>Inventory Control System</p>Posted by <a href="https://www.facebook.com/pages/School-Management-System/815488331878130">E-karobar</a> on Thursday, May 7, 2015</blockquote></div></div>
								</div>
							</div>
							<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 vertical-margin center">
								<a href="javascript:void(0)" class="btn btn-primary">Inventory Control </a>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!--/.box-->
		</div>
		<!--/.container-->
	</section>
	<!--/#portfolio-->





	<?php $this->load->view('e-karobar/contactus'); ?>
	<?php $this->load->view('e-karobar/common/footer'); ?>

	<?php $this->load->view('e-karobar/common/common_include_body'); ?>

	<script type="text/javascript">

	function showVideo(title, url){
		$('#global_modal_label').html(title);
		$html = '	<video id="demo_video" width="100%"  controls autoplay>'
			  	+'		<source src="'+url+'" type="video/mp4">'
				+'			Your browser does not support the video tag.'
				+'	</video>';
		$('#global_modal_body').html($html);
		$('#videos_modal').modal('show');
	}
	
	
	function playPause() { 
	var demo_video = document.getElementById("demo_video"); 
		if (demo_video.paused) 
			demo_video.play(); 
		else 
			demo_video.pause(); 
	} 
	function pause_demo_video(){
		var demo_video = document.getElementById("demo_video"); 
		demo_video.pause();
	}
    
    </script>



	<div class="modal fade " id="videos_modal" tabindex="-1" role="dialog"
		aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog" style="margin: 85px auto;">
			<div class="modal-content">

				<div class="modal-body" id="global_modal_body"></div>
				<div class="modal-footer">
					<h4 class="modal-title pull-left" id="global_modal_label"></h4>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						onclick="pause_demo_video();">Close</button>
				</div>
			</div>
		</div>
	</div>





</body>
</html>
